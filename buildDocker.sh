#! /bin/bash
set -e

echo "Removing imgaes if any..."
docker rmi -f envoy-test:v1
docker rmi -f request-responder:v1

echo "Building images..."
docker build envoy/dockerImage/. -t envoy-test:v1  
docker build requestResponder/dockerImage/. -t request-responder:v1
echo "New images:"
docker images

echo "Loading images to kind..."
kind load docker-image envoy-test:v1
kind load docker-image request-responder:v1
kind load docker-image redis