kind create cluster --config kind-cluster-config.yaml
bash buildDocker.sh
kubectl apply -f namespaces.yaml
kubectl apply -f rate-limiter
kubectl apply -f redis
kubectl apply -f requestResponder
kubectl port-forward svc/envoy-proxy-service -n learn 1234 &
bash sendRequest.sh