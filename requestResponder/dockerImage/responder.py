from flask import Flask
import time
app = Flask(__name__)
requests = 0 
previousTime=time.time()
@app.route("/")
def index():
    global requests
    global previousTime
    nowTime=time.time()
    if previousTime-nowTime >= 60:
        requests=0
    previousTime = nowTime
    requests = requests +1 
    return str(requests)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
